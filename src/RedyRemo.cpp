/*
 * Redy remote monitoring upnp port mapper.
 *
 * Configures upnp to redirect an external port to port 22 for ssh access.
 * Periodically sends a packet to a given server with:
 * - Gateway ID (mac).
 * - Port for external access.
 *
 * Aditional actions:
 * - Reconfigure upnp if external IP or local IP changes.
 */

extern "C" {
#include "redyremo_aux.h"
}

#include <unistd.h>
#include <string>
#include <sstream>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "configuration.h"

const int kN_MINUTES_SLEEP = 7;
const std::string kCFG_FILENAME("./config");
const std::string kCFG_LOCAL_IP("local_ip");
const std::string kCFG_EXTERNAL_IP("external_ip");
const std::string kCFG_INTERNAL_PORT("internal_port");
const std::string kCFG_EXTERNAL_PORT("external_port");
const std::string kCFG_DESCRIPTION("description");
const std::string kCFG_LEASE_TIME("lease_time");
const std::string kCFG_MAC("mac");  // filled in automagically
const std::string kCFG_ANNOUNCE_URL("announce_url");
const std::string kCFG_ANNOUNCE_PORT("announce_port");
const std::string kMAC_PATH("/sys/class/net/eth0/address");
const std::string kPROTO("TCP");
const std::string kIFACE("eth0");

std::string
getMAC()
{
  std::ifstream f(kMAC_PATH.c_str());
  std::string mac;
  std::getline(f, mac);
  return mac;
}

bool
loadConfiguration(configuration::data& cfg_data)
{
  // Load previous configuration from file
  std::ifstream f(kCFG_FILENAME.c_str());
  f >> cfg_data;
  f.close();

  // Validate previous configuration
  if (cfg_data.find(kCFG_LOCAL_IP) == cfg_data.end()
          || cfg_data.find(kCFG_EXTERNAL_IP) == cfg_data.end()
          || cfg_data.find(kCFG_DESCRIPTION) == cfg_data.end()
          || cfg_data.find(kCFG_INTERNAL_PORT) == cfg_data.end()
          || cfg_data.find(kCFG_LEASE_TIME) == cfg_data.end()
          || cfg_data.find(kCFG_EXTERNAL_PORT) == cfg_data.end()) return false;

  return true;

}

void
saveConfig(configuration::data& cfg_data)
{
  std::ofstream f(kCFG_FILENAME.c_str());
  f << cfg_data;
  f.close();
}

bool
getCurrentNetworkInfo(char* externalIPAddress,
                      char* lanaddr,
                      struct UPNPUrls* urls,
                      struct IGDdatas* data)
{
  struct UPNPDev * devlist = 0;
  const char * multicastif = kIFACE.c_str();
  const char * minissdpdpath = 0;
  int error = 0;
  int ipv6 = 0;

  devlist = upnpDiscover(2000, multicastif, minissdpdpath, 0/*sameport*/, ipv6,
                         &error);

  if (!devlist) {
    std::cout << "No IGD UPnP Device found on the network !\n";
    std::cout << "upnpDiscover() error code=%d\n" << error << "\n";
    return false;
  }

  struct UPNPDev* device;
  printf("List of UPNP devices found on the network :\n");
  for (device = devlist; device; device = device->pNext) {
    printf(" desc: %s\n st: %s\n\n", device->descURL, device->st);
  }

  int i = 1;
  char _lanaddr[64]; /* my ip address on the LAN */
  i = UPNP_GetValidIGD(devlist, urls, data, _lanaddr, sizeof(_lanaddr));
  if (i < 0) {
    printf("No valid UPNP Internet Gateway Device found.\n");
    freeUPNPDevlist(devlist);
    return false;
  }

  strcpy(lanaddr, _lanaddr);

  switch (i)
    {
  case 1:
    printf("Found valid IGD : %s\n", urls->controlURL);
    break;
  case 2:
    printf("Found a (not connected?) IGD : %s\n", urls->controlURL);
    printf("Trying to continue anyway\n");
    break;
  case 3:
    printf("UPnP device found. Is it an IGD ? : %s\n", urls->controlURL);
    printf("Trying to continue anyway\n");
    break;
  default:
    printf("Found device (igd ?) : %s\n", urls->controlURL);
    printf("Trying to continue anyway\n");
    }
  printf("Local LAN ip address : %s\n", lanaddr);

  int r = UPNP_GetExternalIPAddress(urls->controlURL, data->first.servicetype,
                                    externalIPAddress);
  if (r != UPNPCOMMAND_SUCCESS) {
    printf("GetExternalIPAddress failed.\n");
    freeUPNPDevlist(devlist);
    return false;
  } else {
    printf("ExternalIPAddress = %s\n", externalIPAddress);
  }

  freeUPNPDevlist(devlist);
  return true;
}

template<typename T>
  T
  stringToNumber(const std::string& Text, T defValue = T())
  {
    std::stringstream ss;
    for (std::string::const_iterator i = Text.begin(); i != Text.end(); ++i)
      if (isdigit(*i) || *i == 'e' || *i == '-' || *i == '+' || *i == '.')
        ss << *i;
    T result;
    return ss >> result ? result : defValue;
  }

template<typename T>
  std::string
  numberToString(T Number)
  {
    std::stringstream ss;
    ss << Number;
    return ss.str();
  }

bool
update_upnp_on_ip_change(configuration::data& cfg_data)
{
  char external_ip[40];
  char internal_ip[64]; /* my ip address on the LAN */
  struct UPNPUrls urls;
  struct IGDdatas data;
  getCurrentNetworkInfo(external_ip, internal_ip, &urls, &data);

  // See if network has changed
  bool is_network_changed = false;
  if (cfg_data[kCFG_EXTERNAL_IP].compare(external_ip) != 0) {
    std::cout << "External IP has changed, was " << cfg_data[kCFG_EXTERNAL_IP]
            << ", now is " << external_ip << std::endl;
    is_network_changed = true;  // doesn't make sense?
  }

  if (cfg_data[kCFG_LOCAL_IP].compare(internal_ip) != 0) {
    std::cout << "Internal IP has changed, was " << cfg_data[kCFG_LOCAL_IP]
            << ", now is " << internal_ip << std::endl;
    is_network_changed = true;
  }

  if (is_network_changed) {
    // Try to undo previous upnp
    std::cout << "Attempting to remove previous redirect: " << cfg_data
            << std::endl;
    if (RemoveRedirect(&urls, &data, cfg_data[kCFG_EXTERNAL_PORT].c_str(),
                       kPROTO.c_str(), NULL) < 0) {
      printf("Failed to remove redirect.\n");
    }

    cfg_data[kCFG_EXTERNAL_IP] = external_ip;
    cfg_data[kCFG_LOCAL_IP] = internal_ip;

    // Reconfigure UPNP
    unsigned int port = stringToNumber(cfg_data[kCFG_EXTERNAL_PORT], 10000);

    for (int i = 0; i < 1000; ++i) {
      const std::string port_str = numberToString(port + i);

      std::cout << "Reconfiguring UPNP attempt #" << i << " on port #"
              << port_str << std::endl;
      if (SetRedirectAndTest(&urls, &data, cfg_data[kCFG_LOCAL_IP].c_str(),
                             cfg_data[kCFG_INTERNAL_PORT].c_str(),
                             port_str.c_str(), kPROTO.c_str(),
                             cfg_data[kCFG_LEASE_TIME].c_str(),
                             cfg_data[kCFG_DESCRIPTION].c_str(), 0) < 0) {
        printf("Failed setting redirect.\n");
      } else {
//        if (SetRedirectAndTest(&urls, &data, cfg_data[kCFG_LOCAL_IP].c_str(),
//                               "60000",
//                               "60000",
//                               "UDP",
//                               cfg_data[kCFG_LEASE_TIME].c_str(),
//                               cfg_data[kCFG_DESCRIPTION].c_str(), 0) < 0) {
//          printf("Failed setting UDP redirect.\n");
//          RemoveRedirect(&urls, &data, "60000",
//                                 "UDP", NULL);
//        } else {
        cfg_data[kCFG_EXTERNAL_PORT] = port_str;
        printf("Successfully reconfigured UPNP\n");
        saveConfig(cfg_data);
        FreeUPNPUrls(&urls);
        return true;
//        }
      }

      boost::this_thread::sleep(boost::posix_time::seconds(10));
    }
  } else {
    printf("Network settings haven't changed, not going to reconfigure upnp.\n");
    FreeUPNPUrls(&urls);
    return true;
  }

  printf("Gave up setting redirect.\n");
  FreeUPNPUrls(&urls);
  return false;
}

int
main(int argc, char ** argv)
{
  configuration::data cfg_data;

  if (loadConfiguration(cfg_data)) {
    std::cout << "Valid previous configuration found: " << std::endl << cfg_data
            << std::endl;
  } else {
    std::cout << "Invalid configuration." << std::endl;
    return -1;
  }

  std::string mac = getMAC();
  cfg_data[kCFG_MAC] = mac;
  saveConfig(cfg_data);

  using boost::asio::ip::udp;
  boost::asio::io_service io_service;
  udp::resolver resolver(io_service);
  udp::endpoint receiver_endpoint;
  udp::socket socket(io_service);
  socket.open(udp::v4());
  bool is_resolved = false;
  while (!is_resolved) {
    try {
      is_resolved = true;
      udp::resolver::query query(udp::v4(), cfg_data[kCFG_ANNOUNCE_URL],
                                 cfg_data[kCFG_ANNOUNCE_PORT]);
      receiver_endpoint = *resolver.resolve(query);
    } catch (std::exception& e) {
      std::cerr << "Failed to resolve " << cfg_data[kCFG_ANNOUNCE_URL] << ":"
              << cfg_data[kCFG_ANNOUNCE_PORT] << ", error:" << e.what()
              << std::endl;
      is_resolved = false;
      sleep(5);
    }
  }

  while (true) {
    update_upnp_on_ip_change(cfg_data);

    std::stringstream data_to_send;
    data_to_send << "mac=" << cfg_data[kCFG_MAC] << ";external_ip="
            << cfg_data[kCFG_EXTERNAL_IP] << ";external_port="
            << cfg_data[kCFG_EXTERNAL_PORT];
    std::cout << "Sending data: " << data_to_send.str() << std::endl;
    socket.send_to(boost::asio::buffer(data_to_send.str()), receiver_endpoint);

    // Sleep
    for (int i = kN_MINUTES_SLEEP; i > 0; --i) {
      boost::this_thread::sleep(boost::posix_time::seconds(60));
    }
  }

  return 0;
}

